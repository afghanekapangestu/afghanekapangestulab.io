---
title: Links

links:
  - title: GitHub
    description: GitHub is the world's largest software development platform.
    website: https://github.com
    image: https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png
  - title: TypeScript
    description: TypeScript is a typed superset of JavaScript that compiles to plain JavaScript.
    website: https://www.typescriptlang.org
    image: ts-logo-128.jpg
  - title: Facebook
    description: Facebook adalah sebuah perusahaan mantap
    website: https://www.facebook.com
    image: https://upload.wikimedia.org/wikipedia/commons/0/06/Facebook.svg

menu:
    main: 
        weight: -50
        params:
            icon: link

comments: false
---


`image` field accepts both local and external images.